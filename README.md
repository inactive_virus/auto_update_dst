# 饥荒服务端自动更新脚本
# Don't Starve Together Dedicated Server Auto-update Script 

一个检查饥荒联机版服务端更新的脚本，并能自动重启服务器。搭配cron定时检查。

在clone之后可以直接运行 ./crontab_add.sh 把脚本增加到cron

A shell script which check for updates of Don't Starve Together Dedicated Server and restarts the server in needs. May work with cron.

run './crontab_add.sh eng' after cloning to simply add this script to cron.

文件 / Files:
* auto_update_dst.sh : 中文版脚本
* auto_update_dst_eng.sh : English version
* update.log : steamcmd 最后一次的输出日志
* auto_update_dst.log : 本脚本的输出日志(使用crontab_add.sh默认产生)

感谢你铃提供的参考轮子（而这是个新轮子）

通过steamcmd的输出信息决定动作

大概可移植性会更好一些（用来更新别的服务端的话

增加了自动添加crontab的简易脚本

增加了配套的服务端启动脚本